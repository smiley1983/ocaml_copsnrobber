type tile = [ `Cop | `Robber ];;

(*type dir = [ `N | `E | `S | `W | `NE | `SE | `NW | `SW ];;*)

class timer limit =
  object(self)
    val mutable go_time = 0.0
    val limit = limit
    method set_go_time = go_time <- Sys.time ()
    method time_remaining =
      1000. *. ((limit /. 1000.) -. ((Sys.time ()) -. go_time))
  end
;;

type state =
 {
  mutable my_id : tile;
  mutable cop : (int * int) array;
  mutable robber: int * int;
  timer : timer;
 }
;;

