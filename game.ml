open Td;;

let in_bounds (row, col) =
  row >= 0
  && col >= 0
  && row < Const.height
  && col < Const.width
;;

let cardinal8_offset =
  [
    (0, 1);
    (1, 1);
    (1, 0);
    (1, -1);
    (0, -1);
    (-1, -1);
    (-1, 0);
    (-1, 1);
  ]
;;

let cardinal_offset =
  [
    (0, 1);
    (1, 0);
    (0, -1);
    (-1, 0);
  ]
;;

let rec neighbors (row, col) = function
  | [] -> []
  | (off_row, off_col) :: tail ->
      let tr, tc = row + off_row, col + off_col in
        if in_bounds (tr, tc) then
         (
          (tr, tc) :: neighbors (row, col) tail
         )
        else neighbors (row, col) tail
;;

let valid_moves state loc =
  match state.my_id with
  | `Cop -> neighbors loc cardinal_offset
  | `Robber -> neighbors loc cardinal8_offset
;;

let my_pieces state =
  match state.my_id with
  | `Cop -> state.cop
  | `Robber -> [| state.robber |]
;;

