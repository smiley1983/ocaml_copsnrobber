open Td;;

(* read 2 lines from stdin, return the list of strings *)
let read_lines () =
  let line_count = ref 0 in
  let rec read_loop acc =
    let line = read_line () in
    line_count := !line_count + 1;
    if !line_count >= 2 then
     (
      List.rev (line :: acc)
     )
    else
      read_loop (line :: acc)
  in
  try Some (read_loop []) with End_of_file -> None
;;

(* tokenizer from rosetta code *)
let split_char sep str =
  let string_index_from i =
    try Some (String.index_from str i sep)
    with Not_found -> None
  in
  let rec aux i acc = match string_index_from i with
    | Some i' ->
        let w = String.sub str i (i' - i) in
        aux (succ i') (w::acc)
    | None ->
        let w = String.sub str i (String.length str - i) in
        List.rev (w::acc)
  in
  aux 0 []
;;

let parse_char = function
  | 'R' -> `Robber
  | _ -> `Cop
;;

(* parse an input string consisting of a single token *)
let one_token state t =
  match String.length t with
  | 1 -> state.my_id <- parse_char t.[0]
  | _ -> ()
;;

let eight_token state t =
  state.robber <- (t.(0), t.(1));
  state.cop.(0) <- (t.(2), t.(3));
  state.cop.(1) <- (t.(4), t.(5));
  state.cop.(2) <- (t.(6), t.(7));
;;

let int_array_of_string_list tokens =
  Array.map (fun v -> int_of_string v) (Array.of_list tokens)
;;

(* parse a line of input *)
let parse_line state l =
  let tokens = split_char ' ' l in
  match List.length tokens with
  | 1 -> one_token state (List.nth tokens 0)
  | 8 -> eight_token state (int_array_of_string_list tokens)
  | _ -> ()
;;

let update state =
  match read_lines () with
  | None -> exit 0
  | Some ll ->
      List.iter (fun l -> parse_line state l) ll
;;

let issue_order (row, col) =
  Printf.printf "%d %d " row col
;;

let finish_turn () =
  Printf.printf "\n"; flush stdout
;;
