open Td;;

(* pick a random item from a list *)
let random_choice l =
  let num = List.length l in
  let choice = Random.int num in
    List.nth l choice
;;

(* get the list of valid moves and return a random one *)
let engine state = 
  let my_pieces = Game.my_pieces state in
  let valid = Array.map (Game.valid_moves state) my_pieces in
  let chosen = Array.map random_choice valid in
    Array.iter Io.issue_order chosen;
(*    Io.finish_turn (); *)
;;

