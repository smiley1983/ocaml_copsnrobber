This is a starter package for hackerrank's Cops and Robber challenge.

It can be packaged into one file using the included one_file program.

To build everything:

ocamlbuild main.native 		# check that the source works

./main.native < input.txt	# test input

./main.native < input2.txt	# test input2

ocamlbuild one_file.native	# build the one_file program

./one_file.native		# invoke it

ocamlbuild output.native	# check that you can build the result

The output.ml file should compile correctly when submitted to hackerrank.

