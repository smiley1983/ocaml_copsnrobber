
let read_file filename =
  let in_chan = open_in filename in
  let lines = ref [] in
  try
    while true; do
      lines := input_line in_chan :: !lines
    done; []
  with End_of_file ->
    close_in in_chan;
    List.rev !lines;
;;

let indent_lines ll =
   List.map (fun l -> "  " ^ l ^ "\n") ll
;;

let output_lines out_chan ll =
   List.iter (fun l -> output_string out_chan l) ll
;;

let file_to_module filename out_chan =
  let module_name = Filename.chop_extension filename in
  let first_letter = module_name.[0] in
    module_name.[0] <- Char.uppercase first_letter;
  let header = ["module " ^ module_name ^ " = struct\n"] in
  let footer = ["\nend;;\n\n"] in
  let lines = read_file filename in
  let text = indent_lines lines in
    output_lines out_chan header;
    output_lines out_chan text;
    output_lines out_chan footer;
;;

let one_file outfile =
  let modules = 
    ["td.ml"; "const.ml" ; "debug.ml"; "io.ml"; "game.ml"; "ai.ml"; 
     "main.ml"] 
  in
  let out_chan = open_out outfile in
  List.iter (fun filename -> file_to_module filename out_chan) modules;
  close_out out_chan
;;

one_file "output.ml";;

